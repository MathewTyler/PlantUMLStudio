PlantUMLStudio
==============

PlantUML Studio is a fork of the [PlantUML Editor](http://code.google.com/p/plantumleditor/) project. It contains various enhancements and a UI that more closely resembles Visual Studio.
